<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Laravel 10

Petunjuk Penggunaan :
- Clone Aplikasi

- composer install

- php artisan key:generate

- Buat DATABASE dengan nama crud_laravel10

- Buat file .env dengan nama DATABASE crud_laravel10

- JALANKAN php artisan migrate

- JALANKAN php artisan db:seed --class=MasterKaryawanSeeder sebagai contoh beberapa data

- Jalankan Aplikasi dilocal Anda dengan mengetikan 
php artisan serve atau php -S localhost:8089 -t public

- Add master karyawan terlebih dahulu di menu Master

- Kemudian add training karyawan di menu Data Training Karyawan

![Kas RT](public/screnshoot/masterKaryawan.png)

![Kas RT](public/screnshoot/tambahMasterKaryawan.png)

![Kas RT](public/screnshoot/editMasterKaryawan.png)

![Kas RT](public/screnshoot/trainingKaryawan.png)

![Kas RT](public/screnshoot/addTrainingKaryawan.png)

![Kas RT](public/screnshoot/editTrainingKaryawan.png)
