<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Pegawai;
use App\Models\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TrainingController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function karyawanTraining()
    {
        $data = DB::table('pegawais')->select('nip', 'nama_karyawan')->get();
        return view('posts.karyawanTraining', ['getNip' => $data]);
    }

    public function dataTraining()
    {
        $data = DB::table('pegawais')->select('nip', 'nama_karyawan')->get();
        return view('posts.training', ['getNip' => $data]);
    }

    public function datatablesTraining(Request $request)
    {
        $data = DB::table('trainings as a')
            ->select('a.*', 'b.nip', 'b.nama_karyawan')
            ->leftJoin('pegawais as b', 'b.nip', '=', 'a.nip')
            ->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                    <div class="hidden-sm hidden-xs btn-group">
                        <button class="btn btn-xs btn-info btn-edit" onclick="editData('.$row->id.')" title="Edit Data">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </button>
                    </div>
                    ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeKaryawanTraining(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'nip' => 'required',
                'jenis' => 'required',
                'tanggal_sertifikat' => 'required'
            );
            $message = array(
                'nip.required' => 'Nip wajib diisi !<br>',
                'jenis.required' => 'Jenis Training wajib diisi !<br>',
                'tanggal_sertifikat.required' => 'Tanggal sertifikat wajib diisi !'
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            // -------------------------------------------------

            $data = new Training();
            $data->nip = $request->nip;
            $data->jenis = $request->jenis;
            $data->tanggal_sertifikat = $request->tanggal_sertifikat;
            $data->keterangan = isset($request->keterangan) ? $request->keterangan : '';
            $data->save();
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function editKaryawanTraining($id)
    {
        $getData = Training::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function updateKaryawanTraining(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'edit_nip' => 'required',
                'edit_jenis' => 'required',
                'edit_tanggal_sertifikat' => 'required'
            );
            $message = array(
                'edit_nip.required' => 'Nip wajib diisi !<br>',
                'edit_jenis.required' => 'Jenis Training wajib diisi !<br>',
                'edit_tanggal_sertifikat.required' => 'Tanggal sertifikat wajib diisi !'
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }

            Training::where('id', $request->id)->update([
                'nip' => $request->edit_nip,
                'jenis' => $request->edit_jenis,
                'tanggal_sertifikat' => $request->edit_tanggal_sertifikat,
                'keterangan' => isset($request->edit_keterangan) ? $request->edit_keterangan : ''
            ]);
    
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }
}
