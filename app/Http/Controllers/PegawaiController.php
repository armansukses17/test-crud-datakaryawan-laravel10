<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Pegawai;
use App\Models\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PegawaiController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function master()
    {
        return view('posts.masterKaryawan');
    }

    public function dataKaryawanTraining()
    {
        $data = DB::table('pegawais')->select('nip', 'nama_karyawan')->get();
        return view('posts.karyawan', ['getNip' => $data]);
    }

    public function datatablesKaryawan(Request $request)
    {
        $data = Pegawai::all();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                    <div class="hidden-sm hidden-xs btn-group">
                        <button class="btn btn-xs btn-info btn-edit" onclick="editData('.$row->id.')" title="Edit Data">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </button>
                    </div>
                    ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function storeMasterPegawai(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'nip' => 'required',
                'nama_karyawan' => 'required',
                'jabatan' => 'required'
            );
            $message = array(
                'nip.required' => 'Nip wajib diisi !<br>',
                'nama_karyawan.required' => 'Nama karyawan wajib diisi !<br>',
                'jabatan.required' => 'Jabatan wajib diisi !'
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            // -------------------------------------------------

            $data = new Pegawai();
            $data->nip = $request->nip;
            $data->nama_karyawan = $request->nama_karyawan;
            $data->jabatan = $request->jabatan;
            $data->save();
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function editMasterPegawai($id)
    {
        $getData = Pegawai::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function updateMasterPegawai(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'edit_nip' => 'required',
                'edit_nama' => 'required',
                'edit_jabatan' => 'required'
            );
            $message = array(
                'edit_nip.required' => 'Nip wajib diisi !<br>',
                'edit_nama.required' => 'Nama karyawan wajib diisi !<br>',
                'edit_jabatan.required' => 'Jabatan wajib diisi !'
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }

            Pegawai::where('id', $request->id)->update([
                // 'nip' => $request->edit_nip,
                'nama_karyawan' => $request->edit_nama,
                'jabatan' => $request->edit_jabatan
            ]);
    
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }
}
