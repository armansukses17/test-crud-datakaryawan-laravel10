<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Pegawai;

class MasterKaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
				'nip' => '2323232323001',
				'nama_karyawan' => 'Bondan',
                'jabatan' => 'Project Manajer'
            ],
            [
				'nip' => '3333333334556',
				'nama_karyawan' => 'Ayu',
                'jabatan' => 'Admin Project'
            ],
        ];
  
        foreach ($data as $key => $value) {
            Pegawai::create($value);
        }
    }
}
