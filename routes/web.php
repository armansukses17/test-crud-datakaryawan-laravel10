<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('posts.masterKaryawan');
});
// Route::resource('/karyawan', \App\Http\Controllers\PegawaiController::class);
Route::get('/master-karyawan', [\App\Http\Controllers\PegawaiController::class, 'master']);
Route::get('/datatablesKaryawan', [\App\Http\Controllers\PegawaiController::class, 'datatablesKaryawan']);
Route::post('/saveDataMasterPegawai', [\App\Http\Controllers\PegawaiController::class, 'storeMasterPegawai']);
Route::get('/editMasterPegawai/edit/{id}', [\App\Http\Controllers\PegawaiController::class, 'editMasterPegawai']);
Route::post('/updateMasterPegawai', [\App\Http\Controllers\PegawaiController::class, 'updateMasterPegawai']);

Route::get('/karyawan-training', [\App\Http\Controllers\TrainingController::class, 'karyawanTraining']);
Route::get('/datatablesTraining', [\App\Http\Controllers\TrainingController::class, 'datatablesTraining']);
Route::post('/saveDataKaryawanTraining', [\App\Http\Controllers\TrainingController::class, 'storeKaryawanTraining']);
Route::get('/editKaryawanTraining/edit/{id}', [\App\Http\Controllers\TrainingController::class, 'editKaryawanTraining']);
Route::post('/updateKaryawanTraining', [\App\Http\Controllers\TrainingController::class, 'updateKaryawanTraining']);
