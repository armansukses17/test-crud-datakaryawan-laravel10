<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Testing</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body style="background: lightgray">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Master </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ '/master-karyawan' }}">Master Karyawan</a>
                                </div>
                            </div>
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ '/karyawan-training' }}">Data Training Karyawan</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    
                    <div>
                        <h1 class="text-center my-4">Data Training Karyawan</h1>
                        <hr>
                    </div>
                    <div class="card border-0 shadow-sm rounded">
                        <div class="card-body">
                        <a href="#modalAddData" role="button" class="btn btn-md btn-success mb-3" data-toggle="modal"> Tambah </a>
                            <table class="table table-hover table-bordered" id="tblData">
                                <thead>
                                    <tr>
                                        <td width="5px">No</td>
                                        <td>NIP</td>
                                        <td>NAMA</td>
                                        <td>JENIS</td>
                                        <td>TANGGAL SERTIFIKAT</td>
                                        <td>KETERANGAN</td>
                                        <td width="12px">AKSI</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modalAddData" class="modal animated jackInTheBox" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Training Karyawan</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <div class="msgAlert"></div>

                    <form id="formAddData">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="required">NIP</label>
                                        <select class="chosen-select form-control" name="nip" id="nip">
                                            <option value="">-- Pilih --</option>
                                            @foreach ($getNip as $row)
                                                <option value="{{ $row->nip }}">{{ $row->nip .' - '. $row->nama_karyawan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="required">JENIS TRAINING</label>
                                        <input type="text" name="jenis" id="jenis" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>TANGGAL SERTIFIKAT</label>
                                        <input type="text" name="tanggal_sertifikat" id="tanggal_sertifikat" class="form-control datepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>KETERANGAN</label>""
                                        <textarea class="form-control" rows="4" name="keterangan" id="keterangan" placeholder="Optional ..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- End: modal -->

        
        <div id="modalEditData" class="modal animated jackInTheBox" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <div class="msgAlert"></div>

                    <form id="formEditData">
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="required">NIP</label>
                                        <input type="hidden" name="id" id="keyEdit"> 
                                        <select class="chosen-select form-control" name="edit_nip" id="edit_nip">
                                            <option value="">-- Pilih --</option>
                                            @foreach ($getNip as $row)
                                                <option value="{{ $row->nip }}">{{ $row->nip .' - '. $row->nama_karyawan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>JENIS TRAINING</label>
                                        <input type="text" name="edit_jenis" id="edit_jenis" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>TANGGAL SERTIFIKAT</label>
                                        <input type="text" name="edit_tanggal_sertifikat" id="edit_tanggal_sertifikat" placeholder="YYYY-MM-DD datepicker" class="form-control datepicker">
                                    </div>
                                    <div class="form-group">
                                        <label>KETERANGAN</label>""
                                        <textarea class="form-control" rows="4" name="edit_keterangan" id="edit_keterangan" placeholder="Optional ..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- End: modal -->
    
        <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
        <script>
            $(function() {
                callDataTables();
                $('.datepicker').datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });

            function callDataTables() {
                $("#tblData").dataTable().fnDestroy();
                listDataTables();
            }

            function listDataTables(filterBy='') {
                $('#tblData').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ '/datatablesTraining' }}",
                        data: {
                            // _token: '{{ csrf_token() }}',
                            // filterBy: filterBy
                        },
                        type: "GET"
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                        { data: 'nip', name: 'nip' },
                        { data: 'nama_karyawan', name: 'nama_karyawan' },
                        { data: 'jenis', name: 'jenis' },
                        { data: 'tanggal_sertifikat', name: 'tanggal_sertifikat' },
                        { data: 'keterangan', name: 'keterangan' },
                        { data: 'action', name: 'action' }
                    ],
                    // "order": [[ '1', "desc" ]]
                });
            }
            
            $('#formAddData').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url : "{{ '/saveDataKaryawanTraining' }}",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.s == 'success') {
                            toastr.success(data.m);
                            $('#tblData').DataTable().ajax.reload();
                            $('.modal').modal('hide');
                            $('#formAddData')[0].reset(); // reset form
                        } 
                        else {
                            let messages = '';
                            $.each(data.errors, function(key, value) {
                                messages += '<span class="alert-error">'+value+'</span>';
                            });
                            toastr.error(messages);
                        }
                    },
                    error: function(data) {
                        let messages = '';
                        $.each(data.errors, function(key, value) {
                            messages += '<span class="alert-error">'+value+'</span>';
                        });
                        toastr.error(messages);
                    }
                }); 
            });

            function editData(param) {
                $.ajax({
                    type: "GET",
                    url : "{{ '/editKaryawanTraining/edit/' }}"+param,
                    dataType: "json",
                    success: function(data) {
                        if (data.s == 'success') {
                            $('#modalEditData').modal();
                            $('#keyEdit').val(param);
                            $('#edit_nip').val(data.data[0].nip);
                            $('#edit_tanggal_sertifikat').val(data.data[0].tanggal_sertifikat);
                            $('#edit_jenis').val(data.data[0].jenis);
                            $('#edit_keterangan').val(data.data[0].keterangan);
                        } 
                        else {
                            alert('error');
                        }
                    }
                }); 
            }

            $('#formEditData').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url : "{{ '/updateKaryawanTraining' }}",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.s == 'success') {
                            toastr.success(data.m);
                            $('#tblData').DataTable().ajax.reload();
                            $('.modal').modal('hide');
                        } 
                        else {
                            let messages = '';
                            $.each(data.errors, function(key, value) {
                                messages += '<span class="alert-error">'+value+'</span>';
                            });
                            toastr.error(messages);
                        }
                    }
                }); 
            });
        </script>

    </body>
</html>