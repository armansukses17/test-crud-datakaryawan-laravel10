<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Testing</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body style="background: lightgray">
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Master </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ '/master-karyawan' }}">Master Karyawan</a>
                                </div>
                            </div>
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ '/karyawan-training' }}">Data Training Karyawan</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    
                    <div>
                        <h1 class="text-center my-4">Master Karyawan</h1>
                        <hr>
                    </div>
                    <div class="card border-0 shadow-sm rounded">
                        <div class="card-body">
                        <a href="#modalAddData" role="button" class="btn btn-md btn-success mb-3" data-toggle="modal"> Tambah </a>
                            <table class="table table-hover table-bordered" id="tblData">
                                <thead>
                                    <tr>
                                        <td width="5px">No</td>
                                        <td>NIP</td>
                                        <td>NAMA KARYAWAN</td>
                                        <td>Jabatan</td>
                                        <td width="12px">AKSI</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modalAddData" class="modal animated jackInTheBox" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Master Karyawan</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <div class="msgAlert"></div>

                    <form id="formAddData">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="required">NIP</label>
                                        <input type="text" name="nip" id="nip" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label class="required">NAMA KARYAWAN</label>
                                        <input type="text" name="nama_karyawan" id="nama_karyawan" class="form-control" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>JABATAN</label>
                                        <input type="text" name="jabatan" id="jabatan" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- End: modal -->

        
        <div id="modalEditData" class="modal animated jackInTheBox" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <div class="msgAlert"></div>

                    <form id="formEditData">
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="required">NIP</label>
                                        <input type="hidden" name="id" id="keyEdit"> 
                                        <input type="text" name="edit_nip" id="edit_nip" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Pegawai</label>
                                        <input type="text" name="edit_nama" id="edit_nama" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <input type="text" name="edit_jabatan" id="edit_jabatan" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- End: modal -->
    
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <script>
            $(function() {
                callDataTables();
            });

            function callDataTables() {
                $("#tblData").dataTable().fnDestroy();
                listDataTables();
            }

            function listDataTables(filterBy='') {
                $('#tblData').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ '/datatablesKaryawan' }}",
                        data: {
                            // _token: '{{ csrf_token() }}',
                            // filterBy: filterBy
                        },
                        type: "GET"
                    },
                    columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                        { data: 'nip', name: 'nip' },
                        { data: 'nama_karyawan', name: 'nama_karyawan' },
                        { data: 'jabatan', name: 'jabatan' },
                        { data: 'action', name: 'action' }
                    ],
                    // "order": [[ '1', "desc" ]]
                });
            }
            
            $('#formAddData').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url : "{{ '/saveDataMasterPegawai' }}",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.s == 'success') {
                            toastr.success(data.m);
                            $('#tblData').DataTable().ajax.reload();
                            $('.modal').modal('hide');
                            $('#formAddData')[0].reset(); // reset form
                        } 
                        else {
                            let messages = '';
                            $.each(data.errors, function(key, value) {
                                messages += '<span class="alert-error">'+value+'</span>';
                            });
                            toastr.error(messages);
                        }
                    },
                    error: function(data) {
                        let messages = '';
                        $.each(data.errors, function(key, value) {
                            messages += '<span class="alert-error">'+value+'</span>';
                        });
                        toastr.error(messages);
                    }
                }); 
            });

            function editData(param) {
                $.ajax({
                    type: "GET",
                    url : "{{ '/editMasterPegawai/edit/' }}"+param,
                    dataType: "json",
                    success: function(data) {
                        if (data.s == 'success') {
                            $('#modalEditData').modal();
                            $('#keyEdit').val(param);
                            $('#edit_nip').val(data.data[0].nip);
                            $('#edit_nama').val(data.data[0].nama_karyawan);
                            $('#edit_jabatan').val(data.data[0].jabatan);
                        } 
                        else {
                            alert('error');
                        }
                    }
                }); 
            }

            $('#formEditData').on('submit', function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: "POST",
                    url : "{{ '/updateMasterPegawai' }}",
                    data: formData,
                    dataType: "json",
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.s == 'success') {
                            toastr.success(data.m);
                            $('#tblData').DataTable().ajax.reload();
                            $('.modal').modal('hide');
                        } 
                        else {
                            let messages = '';
                            $.each(data.errors, function(key, value) {
                                messages += '<span class="alert-error">'+value+'</span>';
                            });
                            toastr.error(messages);
                        }
                    }
                }); 
            });
        </script>

    </body>
</html>